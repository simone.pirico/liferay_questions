package me.malagrino.liferayquestions.app;

import static spark.Spark.*;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.malagrino.liferayquestions.exceptions.NoSuchAnswerException;
import me.malagrino.liferayquestions.exceptions.NoSuchQuestionException;
import spark.ModelAndView;
import spark.Session;
import spark.template.velocity.VelocityTemplateEngine;

public class Main {
	private static final int MAX_QUESTIONS = 50;
	
    public static void main(String[] args) {
    	staticFiles.location("/public");
    	
    	InputStream is = Main.class.getResourceAsStream("/liferay_questions.txt");
    	BufferedReader br = new BufferedReader(new InputStreamReader(is));
    	
        List<Question> allQuestions = LiferayQuestionsParser.parse(br);

        get("/questions", (req, res) -> {        	
        	Session session = req.session();
        	//inizializza la sessione se e' stata appena creata
        	if(session.isNew()) init(session, allQuestions);

        	//recupera dalla sessione le domande
        	List<Question> questions = session.attribute("questions");
        	if(questions == null || questions.isEmpty()) {
        		throw new NoSuchQuestionException("Error! No questions found!!!");
        	}
        	//recupera dalla sessione l'indice della domanda
        	int questionIndex = session.attribute("questionIndex");
        	
        	//mostra la domanda se l'indice della domanda è valido
        	if(questionIndex >=0 && questionIndex < MAX_QUESTIONS) {
        		//recupera la domanda corrente
	        	Question question = questions.get(questionIndex);
	        	//setta la domanda come attributo della sessione
	        	session.attribute("question", question);
	        		
	        	//inserisce le variabili nel model da passare al template velocity
	        	Map<String, Object> model = new HashMap<>();
	        	model.put("question", question); //la domanda
	        	model.put("questionNum", questionIndex+1); //il numero della domanda
	        		
	        	boolean isAnswered = session.attribute("isAnswered");
	        	if(isAnswered) {
	        		//la domanda e' stata risposta, dobbiamo mostrare le risposte esatte e sbagliate
	            	List<String> answers = session.attribute("answers");
	            		
	            	boolean isCorrect = question.isCorrect(answers);
	            	if(isCorrect) {
	            		//aggiorna il contatatore delle risposte esatte
	            		int totalRight = session.attribute("totalRight");
	            		session.attribute("totalRight", totalRight+1);
	            	}
	            			
	            	model.put("isAnswered", true);
	            	model.put("isCorrect", isCorrect);
	            	model.put("correctAnswers", question.getCorrectAnswers());
	            	model.put("wrongAnswers", isCorrect?new ArrayList<String>() : answers);
	            	
	            	if(questionIndex == MAX_QUESTIONS-1) {
	            		//e' l'ultima domanda
	            		model.put("lastQuestion", true);
	            	}
	        	}
	        	
	        	return render(model, "templates/question.vm");
	        }
        	else {
        		//le domande sono finite, mostra i risultati
        		res.redirect("/result");
        		return "";
        	}
        });
        
        post("/answer", (req, res) -> {
        	Session session = req.session(false);
     
        	//recupera le risposte selezionate dalla richiesta
        	String[] answersAsParams = req.queryParamsValues("answer");
        	if(answersAsParams == null) {
        		//c'e' un errore! La domanda non e' stata risposta
        		throw new NoSuchAnswerException("Answer is required");
        	}
        	List<String> answers = Arrays.asList(answersAsParams);
        	
        	//setta le risposte nella sessione
        	session.attribute("answers", answers);
        	
        	//la domanda e' stata risposta
        	session.attribute("isAnswered", true);

        	//ritorna alla domanda
        	res.redirect("/questions");
        	return "";
        });
        
        post("/next", (req, res) -> {
        	Session session = req.session(false);
        	//incrementa l'indice delle domande in modo che non si possa ritornare a rispondere alla stessa domanda
        	int questionIndex = session.attribute("questionIndex");
        	session.attribute("questionIndex", questionIndex + 1);
        	session.attribute("isAnswered", false);
        	session.removeAttribute("answers");
        	res.redirect("/questions");
        	return "";
        });
        
        get("/result", (req, res) -> {
        	Session session = req.session(false);
        	
        	int questionIndex = session.attribute("questionIndex");
        	if(questionIndex < MAX_QUESTIONS) {
        		//non è ancora il momento di mostrare i risultati
        		res.redirect("/questions"); //continua a rispondere!
        		return "";
        	}
        	
        	int totalRight = session.attribute("totalRight");
        	double rightPercentage = (totalRight/(double)MAX_QUESTIONS)*100d;
        	
        	Map<String, Object> model = new HashMap<>();
        	model.put("questionsNum", MAX_QUESTIONS);
        	model.put("totalRight", totalRight);
        	model.put("totalWrong", MAX_QUESTIONS-totalRight);
        	model.put("rightPercentage", rightPercentage);
        	
        	return render(model, "templates/result.vm");
        });
        
        post("/newSession", (req, res) -> {
        	Session session = req.session(false);
        	session.invalidate();
        	
        	res.redirect("/questions");
        	return "";
        });
        
        
        exception(NoSuchAnswerException.class, (exception, req, res) -> {
           res.redirect("/questions");
        });
        
        exception(NoSuchQuestionException.class, (exception, req, res) -> {
        	Map<String,Object> model = new HashMap<>();
        	model.put("exceptionMessage", exception.getMessage());
        	
        	res.body(render(model, "templates/exception.vm"));
         });
        
        try {
			Desktop.getDesktop().browse(new URI("http://localhost:4567/questions"));
		} catch (IOException | URISyntaxException ignored) {
			//prova ad aprire il brower all'URL indicato, altrimenti
			//deve essere inserito manualmente
		}
    }
    
    private static String render(Map<String, Object> model, String templatePath) {
        return new VelocityTemplateEngine().render(new ModelAndView(model, templatePath));
    }
    
    private static void init(Session session, List<Question> questions) {  	
		//mescolo le domande
		Collections.shuffle(questions);
		
		
		if(questions.size() > MAX_QUESTIONS) {
			questions = questions.subList(0, MAX_QUESTIONS);
		}
	
		session.attribute("questions", questions);
		session.attribute("questionIndex", 0);
		session.attribute("isAnswered", false);
		session.attribute("totalRight", 0);
    }
}
