package me.malagrino.liferayquestions.app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Question {
	private String question;
	private Map<String, String> answers = new HashMap<>();
	private List<String> correctAnswers = new ArrayList<>();
	
	public Question(String question) {
		this.question = question;
	}
	
	public void addAnswer(String num, String option) {
		answers.put(num, option);
	}
	
	public void addCorrectAnswers(String correctAnswer) {
		correctAnswers.add(correctAnswer);
		Collections.sort(correctAnswers);
	}
	
	public List<String> getCorrectAnswers() {
		return Collections.unmodifiableList(correctAnswers);
	}
	
	public boolean isCorrect(List<String> answers) {
		Collections.sort(answers);
		return correctAnswers.equals(answers);
	}
	
	public String getQuestion() {
		return question;
	}
	
	public Map<String, String> getAnswers() {
		return answers;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(question);
		sb.append("\n\n");
		for(Map.Entry<String,String> e: answers.entrySet()) {
			String num = e.getKey();
			String opt = e.getValue();
			
			sb.append(num);
			sb.append(". ");
			sb.append(opt);
			sb.append("\n");
		}
		
		return sb.toString();
	}
}
