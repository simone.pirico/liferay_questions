package me.malagrino.liferayquestions.exceptions;

public class NoSuchAnswerException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public NoSuchAnswerException() {
		super();
	}

	public NoSuchAnswerException(String message) {
		super(message);
	}
	
}
