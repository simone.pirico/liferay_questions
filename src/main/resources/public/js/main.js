$(document).ready(function() {
	$('#answerButton').attr("disabled", true);
});

function checkAnswers() {
	var checkboxes = $('.options');
	var atLeastOneChecked = false;
	
	checkboxes.each(function(index) {
		if($(this).is(":checked")) {
			atLeastOneChecked = true;
			return;
		}
	});
	
	if(atLeastOneChecked) {
		$('#answerButton').attr("disabled", false);
		$('#answerButton').removeClass("button-disabled").addClass("button");	
	}
	else {
		$('#answerButton').attr("disabled", true);
		$('#answerButton').removeClass("button").addClass("button-disabled");
	}
	
}